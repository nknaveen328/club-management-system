<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Members;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/dashboard');
    }


    public function addMembers(Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'sex' => 'required',
            'phone' => 'required|unique:members|max:10',
            'active_registration' => 'required',
        ]);

        try{

            Members::create($request->all());

            return redirect()->back()->with(['success' => 'Member registration successfull.']);

        }catch(\Exception $e){
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }
    }


    public function showDashboard(){

        $totalRegisteredMembers = Members::where('active_registration',1)->get();

        $maleRegisteredMembers = $totalRegisteredMembers->where('sex','male')->count();
        $femaleRegisteredMembers = $totalRegisteredMembers->where('sex','female')->count();
        $youthCount = $totalRegisteredMembers->where('age','<',40)->count();
        $totalRegisteredMembers = $totalRegisteredMembers->count();

        $unregisteredMembers = Members::where('active_registration',0)->count();

        return view('dashboard')->with(compact('totalRegisteredMembers','youthCount','femaleRegisteredMembers','maleRegisteredMembers', 'unregisteredMembers'));

    }


    public function addAttendance(Request $request){

        $request->validate([
            'date' => 'required',
        ]);

        $date = \DateTime::createFromFormat('d/m/Y', $request['date'])->format('Y-m-d');

        $attendanceCount = Attendance::where('date',$date)->count();

        if($attendanceCount > 0){
            return redirect()->back()->withErrors(['error'=> 'Attendance Entries already updated for the date.']);

        }

        return view('add-attendance')->with(
            [
                'members' => Members::all(),
                'date' => $request['date'],
            ]
        );



    }


    public function addAttendanceDetails(Request $request){

        $request->validate([
            'date' => 'required',
            'attendance' => 'required',
        ]);



        try{
            $date = \DateTime::createFromFormat('d/m/Y', $request['date'])->format('Y-m-d');



            foreach (json_decode($request->attendance) as $a){

                $entry = new Attendance();
                $entry->member_id = $a->id;
                $entry->date = $date;
                $entry->status = $a->status;
                $entry->save();


            }


            return response()->json(['status' => true, 'message' => 'Attendance entries successfully created.']);

        }catch(\Exception $e){

            return response()->json(['status' => false, 'message' => 'Cannot create attendance entries right now. Please try again later']);

        }


    }


    public function getReportOne(Request $request){
        $request->validate([
            'attendance' => 'required',
            'year' => 'required',
        ]);


        try{
            $members = Members::all();

            $attendanceCount = Attendance::where('date','LIKE','%'.$request['year'].'%')->get()->groupBy('date')->count();

            if($attendanceCount == 0){
                return redirect()->back()->withErrors(['error'=> 'Attendance Entries not found for the given year'])->withInput();

            }

            $result = [];

            foreach ($members as $member){
                $presentCount = $member->attendance()->where('status',1)->count();
                $attendancePercent = ($presentCount/$attendanceCount) * 100;

                if($attendancePercent > $request['attendance']){
                    $member->attendance = round($attendancePercent, 2);
                    $result[] = $member;
                }
            }

            return view('reports.report-one')->with([
                'members' => $result,
                'attendance' => $request['attendance'],
                'year' => $request['year'],
            ]);
        }catch(\Exception $e){
            return redirect()->back()->withErrors(['error'=> 'Cannot generate the report right now. '])->withInput();
        }
    }

    public function getReportTwo(Request $request){

        $request->validate([
            'member' => 'required',
            'year' => 'required',
        ]);

        try{
            $member = Members::where('id', $request['member'])->first();

            $attendance = Attendance::where('date','LIKE','%'.$request['year'].'%')->get()->groupBy('date')->count();

            if($attendance == 0){
                return redirect()->back()->withErrors(['error'=> 'Attendance Entries not found for the given year'])->withInput();

            }

            $memberAttendance = $member->attendance()->where('date','LIKE','%'.$request['year'].'%')->where('status',1)->get();

            $attendancePercent = $memberAttendance->count()/$attendance;

            return view('reports.report-two')->with([
                'member' => $member,
                'attendance' => $memberAttendance,
                'year' => $request['year'],
                'percent' => round($attendancePercent*100, 2)
            ]);
        }catch(\Exception $e){
            return redirect()->back()->withErrors(['error'=> 'Cannot generate the report right now. '])->withInput();

        }

    }


    public function getReportThree(Request $request){
        $request->validate(['year' => 'required']);

        try{
            $members = Members::all();

            $attendanceCount = Attendance::where('date','LIKE','%'.$request['year'].'%')->get()->groupBy('date')->count();

            if($attendanceCount == 0){
                return redirect()->back()->withErrors(['error'=> 'Attendance Entries not found for the given year'])->withInput();

            }

            $result = [];

            foreach ($members as $member){
                $presentCount = $member->attendance()->where('status',1)->count();
                $attendancePercent = ($presentCount/$attendanceCount) * 100;

                $member->attendance = round($attendancePercent, 2);
                $result[] = $member;

            }

            return view('reports.report-three')->with([
                'members' => $result,
                'attendance' => $request['attendance'],
                'year' => $request['year'],
            ]);

        }catch(\Exception $e){

            return redirect()->back()->withErrors(['error'=> 'Cannot generate the report right now. '])->withInput();

        }


    }
}
