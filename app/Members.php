<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $fillable = [
        'name',
        'age',
        'sex',
        'phone',
        'active_registration'
    ];


    public function attendance(){
        return $this->hasMany(Attendance::class, 'member_id');
    }
}
