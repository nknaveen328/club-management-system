<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{

    protected $fillable = [
        'date',
        'status'
    ];

    public function members(){
        return $this->belongsTo(Members::class);
    }
}
