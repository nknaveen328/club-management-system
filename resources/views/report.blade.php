@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @if(Route::current()->uri == "report-one")
                            > X% attendance

                            @elseif(Route::current()->uri == "report-two")

                            Member Attendance
                            @else
                            Yearly Report
                        @endif
                    </div>

                    <div class="card-body">
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div style="width: 50%;" class="mx-auto text-danger">{{$error}}</div>
                            @endforeach
                        @endif

                        @if(Route::current()->uri == "report-one")
                            <form method = "POST" action="/get-report-one" style="width: 50%;" class="mx-auto">
                                @csrf
                                <div class="form-group">
                                    <label for="attendance">Attendance %</label>
                                    <input type="number" class="form-control" name="attendance" value="{{old('attendance')}}" id="attendance" required min="1" max="99">
                                </div>

                                <div class="form-group">
                                    <label for="attendance">Year</label>
                                    <input type="number" class="form-control" name="year" id="year" value="{{old('year')}}" required min="2000" max="2050">
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" >
                                </div>
                            </form>

                        @elseif(Route::current()->uri == "report-two")
                            <form method = "POST" action="/get-report-two" style="width: 50%;" class="mx-auto">
                                @csrf
                                <div class="form-group">
                                    <label for="member">Member</label>
                                    <select name="member" id="member" class="form-control">
                                        @foreach(\App\Members::all() as $member)
                                            <option value="{{$member->id}}">{{$member->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="number" class="form-control" name="year" id="year" value="{{old('year')}}" required min="2000" max="2050">
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" >
                                </div>
                            </form>

                            @else
                            <form method = "POST" action="/get-report-three" style="width: 50%;" class="mx-auto">
                                @csrf

                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="number" class="form-control" name="year" id="year" value="{{old('year')}}" required min="2000" max="2050">
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" >
                                </div>
                            </form>
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){

            $('#attendance').on('keypress', function(){
                console.log($(this).val());
                return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
            });
        });
    </script>

@endsection

