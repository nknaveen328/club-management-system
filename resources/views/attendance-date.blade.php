@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Select Date</div>
                        <div class="card-body">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div style="width: 50%;" class="mx-auto text-danger">{{$error}}</div>
                                @endforeach
                            @endif
                            <form  style="width: 50%" class="mx-auto" action="add-attendance" method="POST" id="attendance-form" name="attendance-form">
                                @csrf
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="text" name="date" id="date" class="form-control" required autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <input type="submit" class = "btn btn-primary" required class="form-control">
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

        $(document).ready(function(){
            $("#date").datepicker({
                autoSize: true,
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                beforeShowDay: function(date) {
                    return [date.getDay() === 0,''];
                }
            });


        });
    </script>

@endsection

