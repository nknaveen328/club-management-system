@extends('layouts.app')
<style>
    p{
        margin-bottom: 0px !important;
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p>
                            <span id="header"> {{$member->name}}'s attendance - {{$year}}</span>
                            @if(!empty($attendance))
                                <span class="float-right"><a href="#" id="download-report">Download</a></span>
                            @endif
                        </p>
                        <p><small>{{$member->active_registration ? 'Registered' : 'Unregistered'}}</small></p>
                    </div>

                    <div class="card-body">
                        <table class="table-bordered table">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($attendance as $a)
                                <tr>
                                    <td>{{date('d-m-Y', strtotime($a->date))}}</td>
                                    <td>Present</td>
                                    </td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td>Present Percentage</td>
                                    <td>{{$percent}}%</td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{url('js/export.js')}}"></script>


@endsection

