@extends('layouts.app')
<style>
    .not-selected{
        border-color: red;
        background-color: #ffe7e7;

    }

    .modal-content{
        padding: 3%;
    }
</style>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Attendance for - {{$date}}</div>

                    <div class="card-body">
                        <table class="table-bordered table">
                            <thead>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Phone</th>
                            <th>Member Type</th>
                            <th>Attendance</th>
                            </thead>
                            <tbody>
                                @foreach($members as $member)
                                    <tr>
                                        <td>{{$member->name}}</td>
                                        <td>{{$member->age}}</td>
                                        <td>{{$member->phone}}</td>
                                        <td>{{$member->active_registration ? 'Registered' : 'Unregistered'}}</td>
                                        <td>
                                            <select data-id="{{$member->id}}" name="attendance" class="attendance">
                                                <option value="" selected disabled>Select</option>
                                                <option value="1">Present</option>
                                                <option value="0">Absent</option>
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Add">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="responseModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 id="response">Attendance entries successfully created.</h4>
                </div>
                <a href="/dashboard" class="btn btn-primary mx-auto" style="width: 20%;">Done</a>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('input[type="submit"]').on('click', function(){

                $('select').each(function(){
                    if($(this).find('option:selected').val() == ''){
                        $(this).addClass('not-selected');
                        throw new Error();

                    }else{
                        //console.log($(this).find('option:selected').val())
                    }

                })

                var attendanceDetails = [];

                $('select').each(function(){
                    console.log($(this).find('option:selected').val());
                    // attendanceDetails[$(this).data('id')] = $(this).find('option:selected').val();
                    attendanceDetails.push({id : $(this).data('id'), status : $(this).find('option:selected').val()});
                })

                attendanceDetails = JSON.stringify(attendanceDetails);
                console.log(attendanceDetails);

                var formData = new FormData();
                formData.append('date',{!! json_encode($date) !!})
                formData.append('attendance',attendanceDetails);


                // console.log(JSON.stringify(formData));

                $.ajax({
                    type: "POST",
                    url: "/add-attendance-details",
                    dataType: 'json',
                    processData : false,
                    contentType : false,
                    cache : false,
                    data: formData,
                    success: function(data){
                        if(data.status){
                            $('#modal-title').text('Success');

                        }else{
                            $('#modal-title').text('Oops!');

                        }

                        $('#response').text(data.message);
                        $('#responseModal').modal('show');
                    }
                });

            });



            $('select').on('change', function(){
               $(this).removeClass('not-selected');
            })


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            // $('#responseModal').modal('show');
        });
    </script>

@endsection

