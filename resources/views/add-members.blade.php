@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Members</div>

                    <div class="card-body">


                        <form action="/add-member" method="POST" name="add-members" class="add-members mx-auto" style="width: 50%;">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" value="{{old('name')}}" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="age">Age</label>
                                <input type="number" id="age" min="0" max="99" name="age" value="{{old('age')}}" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="sex">Sex</label>
                                <select name="sex" id="sex" class="form-control">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" id="phone" name="phone" pattern="^\d{10}$" value="{{old('phone')}}" required class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="member-type">Member Type</label>
                                <select name="active_registration" class="form-control" id="active_registration">
                                    <option value="1">Registered</option>
                                    <option value="0">Non Registered</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="submit" class = "btn btn-primary" required class="form-control">
                            </div>

                            @if($errors->any())
                                <p class="text-danger info-text">{{$errors->first()}}</p>
                            @endif
                            @if (\Session::has('success'))
                                <p class="text-success info-text">{!! \Session::get('success') !!}</p>
                            @endif

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('#age').on('keypress', function(){
                console.log($(this).val());
                return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
            });


            $('.form-control').on('keydown', function(){
                $('.info-text').hide();
            });



        });
    </script>

@endsection

