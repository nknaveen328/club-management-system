@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <table class="table-bordered table">
                            <thead>
                                <th>Member Type</th>
                                <th>Count</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Registered Members</td>
                                    <td>{{$totalRegisteredMembers}}</td>
                                </tr>
                                <tr>
                                    <td>Male - Registered Members</td>
                                    <td>{{$maleRegisteredMembers}}</td>
                                </tr>
                                <tr>
                                    <td>Female - Registered Members</td>
                                    <td>{{$femaleRegisteredMembers}}</td>
                                </tr>
                                <tr>
                                    <td>Youth - Registered Members</td>
                                    <td>{{$youthCount}}</td>
                                </tr>
                                <tr>
                                    <td>Unregistered Members</td>
                                    <td>{{$unregisteredMembers}}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){


        });
    </script>

@endsection

