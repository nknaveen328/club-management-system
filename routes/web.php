<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'],  function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/add-member', function (){

        return view('add-members');
    });

    Route::post('add-member', 'HomeController@addMembers');

    Route::get('dashboard', 'HomeController@showDashboard');

    Route::get('/attendance', function (){

        return view('attendance-date');
    });


    Route::post('add-attendance', 'HomeController@addAttendance');

    Route::post('add-attendance-details', 'HomeController@addAttendanceDetails');

    Route::get('report-one', function (){

        return view('report');

    });

    Route::get('report-two', function (){

        return view('report');

    });

    Route::get('report-three', function (){

        return view('report');

    });

    Route::post('get-report-one', 'HomeController@getReportOne');

    Route::post('get-report-two', 'HomeController@getReportTwo');

    Route::post('get-report-three', 'HomeController@getReportThree');

});

