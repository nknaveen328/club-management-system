$(document).ready(function(){
    $('#download-report').on('click', function () {
        var rows = [];

        $('tr').each(function(){

            var children = $(this).children()
            var childTemp = [];

            children.each(function(){
                childTemp.push($(this)[0].innerText);
            });

            var temp = [];
            temp.push(childTemp);

            rows.push(temp);
        });

        let csvContent = "data:text/csv;charset=utf-8,"
            + rows.map(e => e.join(",")).join("\n");
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", $('#header').text()+".csv");
        document.body.appendChild(link);

        link.click();
    })

});